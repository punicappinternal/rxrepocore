# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/rxRepoCore.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/rxRepoCore)

Punicapp Repo core library, which is used in internal projects.

To install the library add: 
 
```
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:rxRepoCore:1.0.1'
   }
```