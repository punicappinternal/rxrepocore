package com.punicapp.rxrepocore;

import com.punicapp.rxrepocore.model.ListWrapper;
import com.punicapp.rxrepocore.model.Wrapper;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;

/**
 * Created by ARTem on 08.08.2017.
 */

public interface IRepository<T> {

    Single<T> save(T item);

    Single<List<T>> saveAll(List<T> items);

    Consumer<T> saveInChain();

    Consumer<List<T>> saveAllInChain();

    Single<T> modifyFirst(Consumer<T> action);

    Single<Integer> removeInChain(List<LocalFilter> filters);

    Single<ListWrapper<T>> fetch(List<LocalFilter> filters, LocalSort sort);

    Single<Wrapper<T>> first(List<LocalFilter> filters);

    ListWrapper<T> instantFetch(List<LocalFilter> filters, LocalSort sort);

    Wrapper<T> instantFirst(List<LocalFilter> filters);

    Single<Long> count(List<LocalFilter> filters);

    Long instantCount(List<LocalFilter> filters);
}
