package com.punicapp.rxrepocore;


import java.util.List;

public class LocalSort<T> {
    private List<String> property;
    private List<T> sort;

    public LocalSort(List<String> property, List<T> sort) {
        this.property = property;
        this.sort = sort;
    }

    public List<String> getProperty() {
        return property;
    }

    public void setProperty(List<String> property) {
        this.property = property;
    }

    public List<T> getSort() {
        return sort;
    }

    public void setSort(List<T> sort) {
        this.sort = sort;
    }
}
