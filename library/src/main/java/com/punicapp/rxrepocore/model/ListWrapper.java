package com.punicapp.rxrepocore.model;

import java.util.List;

/**
 * Created by ARTem on 22.01.2018.
 */

public class ListWrapper<T> extends Wrapper<List<T>> {

    public ListWrapper(List<T> data) {
        super(data);
    }

    @Override
    protected boolean checkIsEmpty(List<T> data) {
        return super.checkIsEmpty(data) || data.isEmpty();
    }
}
