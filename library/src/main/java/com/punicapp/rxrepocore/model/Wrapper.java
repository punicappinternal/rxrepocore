package com.punicapp.rxrepocore.model;

/**
 * Created by ARTem on 22.01.2018.
 */

public class Wrapper<T> {
    private T data;

    private boolean isEmpty;

    public Wrapper(T data) {
        this.data = data;
        this.isEmpty = checkIsEmpty(data);
    }

    protected boolean checkIsEmpty(T data) {
        return data == null;
    }

    public T getData() {
        return data;
    }

    public boolean isEmpty() {
        return isEmpty;
    }
}
