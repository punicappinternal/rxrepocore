package com.punicapp.rxrepocore;

/**
 * Created by ARTem on 05.05.2017.
 */

public abstract class LocalFilter<T> {
    private String idProp;
    private Object value;
    private Class clazz;
    private Check check;

    public abstract T applyFilter(T query);

    public enum Check {
        Equal,
        NotEqual,
        GreatOrEqual,
        isNull,
        In,
        isNotNull
    }

    public LocalFilter(Class clazz, Check check, String idProp, Object value) {
        this.clazz = clazz;
        this.check = check;
        this.idProp = idProp;
        this.value = value;
    }

    public Check getCheck() {
        return check;
    }

    public String getIdProp() {
        return idProp;
    }

    public Object getValStr() {
        return value;
    }

    public Class getClazz() {
        return clazz;
    }
}
